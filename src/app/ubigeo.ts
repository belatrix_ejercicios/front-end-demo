export class Ubigeo{
   public codigo:string;
   public nombre:string;
   public codigoPadre:string;
   public descripcionPadre:string;

   constructor(cod:any, nom:any, codPadre:any, descPadre:any){
       this.codigo = String(cod);
       this.nombre = String(nom);
       this.codigoPadre = String(codPadre);
       this.descripcionPadre = String(descPadre);
   }
}