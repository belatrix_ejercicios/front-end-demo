import { Component, OnInit } from '@angular/core';
import { NzMessageService, UploadFilter, UploadFile } from 'ng-zorro-antd';
import { Observable, Observer } from 'rxjs';
import { Ubigeo } from './ubigeo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Ubigeos';
  archivo:any;
  departamentos: any[] = [];
  provincias: any[] = [];
  distritos: any[] = [];
  cont:string = '';

  constructor(private msg: NzMessageService){  }

  ngOnInit(){
  }

  fileChanged(file:any){
    this.archivo = file.target.files[0];
    if(this.archivo.type !== 'text/plain'){
      this.crearMensaje('error', 'Debe seleccionar un archivo de extension txt');
    }else{
      this.departamentos = [];
      this.provincias = [];
      this.distritos = [];
      let fileRead = new FileReader();
      fileRead.onload = (e) => {
  
        var contenido = fileRead.result;
        this.procesarContenido(contenido);
      }
      fileRead.readAsText(this.archivo);
    }
    
    
    
  }

  procesarContenido(contenido:any){
    this.cont = contenido;
    let lineas = contenido.split("\n");
    lineas.forEach(element => {
      let elementClean = element.replace(/"|“/g, '');
      let columna = elementClean.split("/");
      if(columna[0].trim() !== '' && columna[1].trim() !== '' && columna[2].trim() !== ''){
        this.agrergarDistrito(columna)
      } else if(columna[1].trim() !== '' && columna[2].trim() === '' ){
        this.agrergarProvincia(columna);
      } else if(columna[0].trim() !== '') {
        this.agrergarDepartamento(columna);
      }
    });
    
  }

  private agrergarDepartamento(dep:any):void{
    let textDep = dep[0].trim();
    let d = textDep.split(" ")
    let ubigeo = new Ubigeo(d[0], d[1], "", "");
    this.departamentos = [...this.departamentos, ubigeo];

  }

  private agrergarProvincia(prov:any){
    let textDep= prov[0].trim();
    let d = textDep.split(" ");
    let textProv = prov[1].trim();
    let p = textProv.split(" ");
    let ubigeo = new Ubigeo(p[0], p[1], d[0], d[1]);
    this.provincias = [...this.provincias, ubigeo];
  }


  private agrergarDistrito(dist:any){
    let textProv = dist[1].trim();
    let p = textProv.split(" ") || [];
    let textDist = dist[2].trim();
    let di = textDist.split(" ");
    let ubigeo = new Ubigeo(di[0], di[1], p[0], p[1]);
    this.distritos = [...this.distritos, ubigeo]
  }

  private crearMensaje(type: string, texto:string): void {
    this.msg.create(type, texto);
  }


}
